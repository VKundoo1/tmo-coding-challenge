import { StocksAppConfig } from '@coding-challenge/stocks/data-access-app-config';

export const environment: StocksAppConfig = {
  production: true,
  apiKey: 'pk_aa2bbae2dc484515a689080ecebd9656',
  apiURL: 'https://sandbox.iexapis.com'
};
